<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function create() {
        return view('pertanyaan.create');
    }

    public function store(Request $request) {
        $request->validate([
            'judul' => 'required|unique:pertanyaan|max:45',
            'isi'   => 'required|max:255'
        ]);

        // $query = DB::table('pertanyaan')->insert([
        //     "judul" => $request["judul"],
        //     "isi"   => $request["isi"]
        // ]);

        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request["judul"];
        // $pertanyaan->isi   = $request["isi"];
        // $pertanyaan->save();

        $pertanyaan = Pertanyaan::create([
            "judul" => $request["judul"],
            "isi"   => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Post Berhasil Disimpan!');
    }

    public function index() { //menampilkan data di index
        // $pertanyaan = DB::table('pertanyaan')->get();
        $pertanyaan = Pertanyaan::all();
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($id) {
        // $tanya = DB::table('pertanyaan')->where('id', $id)->first(); //pakai query bulder

        $tanya = Pertanyaan::find($id);
        return view('pertanyaan.show', compact('tanya'));
    }

    public function edit($id) {
        // $tanya = DB::table('pertanyaan')->where('id', $id)->first();

        $tanya = Pertanyaan::find($id);
        return view('pertanyaan.edit', compact('tanya'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'judul' => 'required|max:45',
            'isi'   => 'required'
        ]);

        // $query = DB::table('pertanyaan')
        //             ->where('id', $id)
        //             ->update([
        //                 'judul' => $request['judul'],
        //                 'isi'   => $request['isi']
        //             ]);

        $update = Pertanyaan::where('id', $id)->update([
            "judul" => $request["judul"],
            "isi"   => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Berhasil Update Pertanyaan!');
    }

    public function destroy($id) {
        // $query = DB::table('pertanyaan')->where('id', $id)->delete();

        Pertanyaan::destroy($id);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dihapus');
    }
}
